//
//  WeatherManager.swift
//  Clima
//
//  Created by Cohen, Dor on 12/10/2020.
//  Copyright © 2020 App Brewery. All rights reserved.
//

import Foundation

protocol WeatherManagerDelegate {
    func didUpdateWeather(weather:WeatherModel)
    func didFailWithError(error: Error)
}

struct WeatherManager{
    let weatherUrl: String = "https://api.openweathermap.org/data/2.5/weather?units=metric"
    let apiKey: String = "49628d77a5487470f370b5772ec81b1c"
    
    var delegate: WeatherManagerDelegate?
    
    // Fetch with city name
    func fetchWeather(city: String){
        // Create the URL
        let url = "\(weatherUrl)&appid=\(apiKey)&q=\(city)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        guard let urlString = url else { return }
        if let url = URL(string: urlString){
            // Create the session
            let session = URLSession(configuration: .default)
            // Create task for the session
            let task = session.dataTask(with: url, completionHandler: {
                (data: Data?, response: URLResponse?, error: Error?) in
                    if let error = error{
                        delegate?.didFailWithError(error: error)
                        return
                    }
                    if let data = data{
                        /*
                        if let dataString = String(data: data, encoding: .utf8){
                            print("Data:",dataString)
                        }
                        */
                        if let weather = self.parseJSON(data: data){
                            delegate?.didUpdateWeather(weather: weather)
                        }
                    }
                
            }  )
            // Start the task
            task.resume()
        }
    }
    
    // Fetch with lon & lat
    func fetchWeather(lon: Double, lat: Double){
        // Create the URL
        let url = "\(weatherUrl)&appid=\(apiKey)&lon=\(lon)&lat=\(lat)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        guard let urlString = url else { return }
        if let url = URL(string: urlString){
            // Create the session
            let session = URLSession(configuration: .default)
            // Create task for the session
            let task = session.dataTask(with: url, completionHandler: {
                (data: Data?, response: URLResponse?, error: Error?) in
                    if let error = error{
                        delegate?.didFailWithError(error: error)
                        return
                    }
                    if let data = data{
                        /*
                        if let dataString = String(data: data, encoding: .utf8){
                            print("Data:",dataString)
                        }
                        */
                        if let weather = self.parseJSON(data: data){
                            delegate?.didUpdateWeather(weather: weather)
                        }
                    }
                
            }  )
            // Start the task
            task.resume()
        }
    }
    
    func parseJSON(data:Data) -> WeatherModel?{
        let decoder = JSONDecoder()
        do{
            let weatherData = try decoder.decode(WeatherData.self, from: data)
            let weatherModel = WeatherModel(weatherId: weatherData.weather[0].id, temp: weatherData.main.temp, feelsLike: weatherData.main.feels_like, city: weatherData.name)
            
            return weatherModel
        }catch{
            delegate?.didFailWithError(error: error)
        }
        return nil
    }
    
}
