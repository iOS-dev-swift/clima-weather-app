//
//  WeatherModel.swift
//  Clima
//
//  Created by Cohen, Dor on 13/10/2020.
//  Copyright © 2020 App Brewery. All rights reserved.
//

import Foundation

struct WeatherModel {
    let weatherId: Int
    let temp: Double
    let feelsLike: Double
    let city: String
    
    var feelsLikeString: String {
        return String(format: "%.1f", feelsLike)
    }
    
    var tempString: String {
        return String(format: "%.1f", temp)
    }
    
    var icon: String {
        switch weatherId {
        case 200...232:
            return "cloud.bolt"
        case 300...321:
            return "cloud.drizzle"
        case 500...531:
            return "cloud.rain"
        case 600...622:
            return "cloud.snow"
        case 701...781:
            return "cloud.fog"
        case 800:
            return "sun.max"
        case 801...804:
            return "cloud.sun"
        default:
            return "sun.max"
        }
    }
}
