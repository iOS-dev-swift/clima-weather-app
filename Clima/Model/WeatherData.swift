//
//  WeatherData.swift
//  Clima
//
//  Created by Cohen, Dor on 13/10/2020.
//  Copyright © 2020 App Brewery. All rights reserved.
//

import Foundation

struct WeatherData: Codable{
    let name: String
    let timezone: Int
    let visibility: Int
    let main: main
    let coord: coord
    let weather: [weather]
}

struct main: Codable{
    let temp: Double
    let feels_like: Double
    let temp_min: Double
    let temp_max: Double
    let pressure: Double
    let humidity: Double
}

struct coord: Codable{
    let lon: Double
    let lat: Double
}

struct weather: Codable {
    let id: Int
    let main: String
    let description: String
    let icon: String
}
